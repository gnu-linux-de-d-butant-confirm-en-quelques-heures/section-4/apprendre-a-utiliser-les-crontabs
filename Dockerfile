#Version 1.0

FROM ubuntu:22.10

ADD *.sh /tmp/

RUN chmod a+x /tmp/*.sh && /tmp/environnement.sh
RUN apt-get update && apt install man nano cron vim -y && yes | unminimize

EXPOSE 80

#CMD ["sleep", "infinity"]
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]