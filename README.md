# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes permettant de créer des tâches automatiques.

Ainsi nous verrons en détail la commande `crontab`.

<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm -p 80:80 --name man jassouline/crontabs:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

<br/>

# Exercice 1

Dans cet exercice, nous allons utiliser la commande `crontab` pour visualiser les crontabs existantes, et en créer de nouvelles.

# Visualiser les crontabs
Grâce à la commande `crontab -l`, visualisez les crontabs existantes.

**Q1: Combien il y a-t-il de crontabs pour l'utilisateur root ?**

<details><summary> Montrer la solution </summary>
<p>
crontab -l
</p>
<p>
no crontab for root
</p>
</details>

# Créer une crontab de sauvegarde

Un serveur nginx a été installé. 

1. Tapez la commande `service nginx start`. 

Vous pouvez observer la page d'accueil en cliquant sur **OPEN PORT** en haut de la page, puis en indiquant le port numéro **80**.

2. Réactualisez plusieurs fois la page, puis allez voir le contenu du fichier */var/log/nginx/access.log* grâce à la commande `cat /var/log/nginx/access.log`

A chaque fois qu'un utilisateur visionne la page d'accueil, cela affiche un nouveau log dans ce fichier, du type :
```
172.17.0.6 - - [17/Aug/2020:14:29:00 +0000] "GET / HTTP/1.1" 200 612 "https://2886795313-80-frugo01.environments.katacoda.com/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36"
```

Cela permet de garder une trace de toutes les activités qui se déroulent sur ce site Internet.

Nous allons créer une crontab dont les spécificités sont les suivantes :
- Doit se lancer toutes les minutes, tous les jours de la semaine
- Doit copier le fichier /var/log/nginx/access.log vers /tmp/sauvegarde/nginx.log

2. Pour créer une nouvelle crontab, utilisez la commande `crontab -e`. Puis sélectionnez l'option 1 pour lancer l'éditeur nano.

<details><summary> Montrer la solution </summary>
<p>
```
* * * * * cp /var/log/nginx/access.log /tmp/sauvegarde/nginx.log
```
</p>
</details>

3. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/crontabs_verify.sh`

Si vous obtenez le message : `OK !` c'est que les actions ont été correctement effectuées.

<br/>

# Conclusion

Tapez la commande `exit` pour sortir du conteneur, ou bien la combinaison de touches `CTRL + c`

